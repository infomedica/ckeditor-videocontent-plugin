(function () {
    // Layouts
    const horizontalLayout = {
        name: 'horizontal',
        videoContainer: 'col-sm-7',
        chaptersContainer: 'col-sm-5'
    };

    const fullWidthColumnClass = 'col-sm-12';
    const verticalLayout = {
        name: 'vertical',
        videoContainer: fullWidthColumnClass,
        chaptersContainer: fullWidthColumnClass
    };

    const defaultLayout = horizontalLayout;

    const layouts = [horizontalLayout, verticalLayout];

    // Common
    const getClassSelector = (className) => {
        return '.' + className;
    };

    // Source: http://garystorey.com/2017/02/27/three-ways-to-create-dom-elements-without-jquery/
    const createElementFromString = (domString) => {
        const html = new DOMParser().parseFromString(domString, 'text/html');
        return html.body.firstChild;
    };

    // Plugin
    const pluginId = 'videoContentPlugin';
    const pluginName = 'videocontent';
    const videoContentTagName = 'video-content';

    const concatPluginName = (name) => {
        return pluginName + name;
    };

    CKEDITOR.plugins.add(pluginName, {
        requires: 'widget',

        init: function (editor) {
            const pluginPath = this.path;

            const getResourcePath = (path) => {
                return pluginPath + path;
            };

            // TODO: Replace static path declaration
            // Assets / Resources
            const getPublicAsset = (path) => {
                return '/generated_assets' + path;
            };

            editor.addContentsCss(getResourcePath('css/main.css'));

            // --- Chapters ---
            const chapterClass = 'video-chapter';
            const chapterSelector = getClassSelector(chapterClass);

            const chapterSecondsClass = 'video-chapter-seconds';
            const chapterSecondsSelector = getClassSelector(chapterSecondsClass);

            const chapterTitleClass = 'video-chapter-title';
            const chapterTitleSelector = getClassSelector(chapterTitleClass);

            const buildChaptersTemplate = (chapterSeconds = 0, chapterTitle = 'Chapter title') => {
                return `<div class="video-chapter">
                            <div class="${chapterSecondsClass}">${chapterSeconds}</div>
                            <div class="${chapterTitleClass}">${chapterTitle}</div>
                        </div>`;
            };

            const generateChapters = (chaptersCount) => {
                let generatedChapters = '';

                for (let i = 0; i < chaptersCount; i++) {
                    generatedChapters += buildChaptersTemplate(i * 10, `Chapter title ${i+1}`) + '\n';
                }

                return generatedChapters;
            };

            const videoContentsPreview =
                `<div class="row row-no-gutters">
                    <div class="${defaultLayout.videoContainer}">
                        <div class="video-preview-placeholder"></div>
                    </div>

                    <div class="video-chapters ${defaultLayout.chaptersContainer}"></div>
                </div>`;

            // Validate functions
            const isEmptyField = (variable) => {
                return typeof variable === 'undefined'
                    || variable === null
                    || variable === '';
            };

            const alertEmpty = (attributeName) => {
                alert(`${attributeName} cannot be empty.`);
            };

            const getValidSeconds = (seconds) => {
                if (typeof seconds === 'string') {
                    seconds = seconds.trim();
                }
                return parseInt(seconds);
            };

            const validateChapterSeconds = (seconds) => {
                let secondsFieldName = 'Chapter seconds';
                if (isEmptyField(seconds)) {
                    alertEmpty(secondsFieldName);
                    return false;
                }

                let secondsInt = getValidSeconds(seconds);
                if (!Number.isInteger(secondsInt)) {
                    alert(`${secondsFieldName} must be an integer.`);
                    return false;
                }

                const minimumSeconds = 0;
                if (secondsInt < minimumSeconds) {
                    alert(`${secondsFieldName} must be greater than or equal to ${minimumSeconds}.`);
                    return false;
                }

                return true;
            };

            const getValidChapterTitle = (title) => {
                return title.trim();
            };

            const validateChapterTitle = (title) => {
                if (isEmptyField(title)) {
                    alertEmpty('Chapter title');
                    return false;
                }

                return true;
            };

            const validateChapter = (seconds, title) => {
                return validateChapterSeconds(seconds)
                    && validateChapterTitle(title);
            };

            // Chapter dialog
            const chapterDialog = function (api) {
                const getChapterElement = function (widget) {
                    return widget.element.$;
                };

                const querySeconds = function (widget) {
                    return getChapterElement(widget).querySelector(chapterSecondsSelector);
                };

                const queryTitle = function (widget) {
                    return getChapterElement(widget).querySelector(chapterTitleSelector);
                };

                return {
                    title: 'Edit Content Chapter',
                    minWith: 800,
                    minHeight: 300,
                    contents: [
                        {
                            id: 'info-chapters',
                            elements: [
                                {
                                    id: 'chapter-dialog-seconds',
                                    type: 'text',
                                    label: 'Seconds',
                                    setup: function(widget) {
                                        let secondsElement = querySeconds(widget);
                                        let seconds = 0;

                                        if (secondsElement !== null && secondsElement.innerHTML !== '') {
                                            seconds = secondsElement.innerHTML;
                                        }

                                        seconds = getValidSeconds(seconds);

                                        this.setValue(seconds);
                                    },
                                    commit: function (widget) {
                                        querySeconds(widget).innerHTML = getValidSeconds(this.getValue());
                                    },
                                    validate: function () {
                                        return validateChapterSeconds(this.getValue());
                                    }
                                },
                                {
                                    id: 'chapter-dialog-title',
                                    type: 'text',
                                    label: 'Title',
                                    setup: function(widget) {
                                        let titleElement = queryTitle(widget);
                                        let title = '';

                                        if (titleElement !== null && titleElement.innerHTML !== '') {
                                            title = titleElement.innerHTML;
                                        }

                                        this.setValue(title);
                                    },
                                    commit: function (widget) {
                                        queryTitle(widget).innerHTML = this.getValue();
                                    },
                                    validate: function () {
                                        return validateChapterTitle(this.getValue());
                                    }
                                }
                            ]
                        }
                    ]
                }
            };

            const chapterDialogName = 'videoContentChapterDialog';
            CKEDITOR.dialog.add(chapterDialogName, chapterDialog);

            const widgetChapterName = 'videoContentChapter';

            // editor.ui.addButton('VideoContentChapter', {
            //     label: 'Insert Video Chapter',
            //     toolbar: 'insert',
            //     command: widgetChapterName
            // });

            editor.widgets.add(widgetChapterName, {
                dialog: chapterDialogName,
                allowedContent: {
                    div: {
                        classes: [chapterSecondsClass, chapterTitleClass]
                    }
                },
                template: buildChaptersTemplate(),
                requiredContent: `div(${chapterSecondsClass}); div(${chapterTitleClass})`,
                // editables: {
                //     seconds: {
                //         selector: '.' + chapterSecondsClass,
                //         allowedContent: 'strong em'
                //     },
                //     title: {
                //         selector: '.' + chapterTitleClass,
                //         allowedContent: 'strong em'
                //     },
                // },
                upcast: function (element) {
                    return element.name === 'div' && element.hasClass(chapterClass);
                },
            });

            // --- Video Content ---
            const videoContentDialog = function (api) {

                const getVideoElement = function (widget) {
                    return widget.element.$.querySelector(videoContentTagName);
                };

                const baseSetup = (attribute) => {
                    return function (widget) {
                        this.setValue(getVideoElement(widget).getAttribute(attribute));
                    }
                };

                const baseCommit = (attribute) => {
                    return function (widget) {
                        getVideoElement(widget).setAttribute(attribute, this.getValue());
                    }
                };

                const baseValidation = () => {
                    return function () {
                        // CKEDITOR.dialog.validate.notEmpty(`${this.label} cannot be empty.`)
                        if (!this.getValue()) {
                            alertEmpty(this.label);
                            return false;
                        }
                    }
                };

                const titleClass = 'video-title';
                const titleSelector = getClassSelector(titleClass);

                const queryTitle = (parentNode) => {
                    return parentNode.querySelector(titleSelector);
                };

                const renderTitle = (widget, title) => {
                    let parentElement = getVideoElement(widget);

                    const tagName = 'h6';

                    let titleElement = queryTitle(parentElement);
                    if (titleElement !== null) {
                        titleElement.innerHTML = title;
                        return;
                    }

                    titleElement = document.createElement(tagName);
                    titleElement.classList.add(titleClass);
                    titleElement.innerHTML = title;

                    parentElement.append(titleElement);
                };

                const dialogId = 'info';
                return {
                    title: 'Edit Video Content',
                    minWith: 800,
                    minHeight: 600,
                    contents: [
                        {
                            id: dialogId,
                            elements: [
                                {
                                    id: concatPluginName('VimeoUrl'),
                                    type: 'text',
                                    label: 'Vimeo Url',
                                    setup: baseSetup('video-url'),
                                    commit: baseCommit('video-url'),
                                    validate: baseValidation()
                                },
                                {
                                    id: concatPluginName('VideoTitle'),
                                    type: 'text',
                                    label: 'Video Title',
                                    setup: function(widget) {
                                        let titleElement = queryTitle(getVideoElement(widget));

                                        let value = '';
                                        if (titleElement !== null) {
                                            value = titleElement.innerHTML;
                                        }

                                        this.setValue(value);
                                    },
                                    commit: function(widget) {
                                        let title = getValidChapterTitle(this.getValue());
                                        renderTitle(widget, title);
                                    },
                                    // validate: baseValidation()
                                },
                                {
                                    id: concatPluginName('Authors'),
                                    type: 'text',
                                    label: 'Authors',
                                    setup: baseSetup('authors'),
                                    commit: baseCommit('authors'),
                                    // validate: baseValidation()
                                },
                                {
                                    id: concatPluginName('References'),
                                    type: 'text',
                                    label: 'References',
                                    setup: baseSetup('references'),
                                    commit: baseCommit('references'),
                                    // validate: baseValidation()
                                },
                                // {
                                //     id: concatPluginName('Chapters'),
                                //     type: 'textarea',
                                //     label: 'Chapters, JSON format',
                                //     setup: function (widget) {
                                //         let chaptersBase64 = getVideoElement(widget).getAttribute('chapters');
                                //         this.setValue(base64.decode(chaptersBase64));
                                //     },
                                //     commit: function (widget) {
                                //         let chaptersBase64 = base64.encode(this.getValue());
                                //
                                //         getVideoElement(widget).setAttribute('chapters', chaptersBase64);
                                //     },
                                //     validate: function() {
                                //         if (!baseValidation()) {
                                //             return false;
                                //         }
                                //
                                //         try {
                                //             let rawChapters = this.getValue();
                                //             let chaptersJSON = JSON.parse(rawChapters);
                                //         } catch (e) {
                                //             alert(this.label + ' error: ' + e.message);
                                //             return false;
                                //         }
                                //     }
                                // },
                                {
                                    type : 'hbox',
                                    label: 'Chapter',
                                    widths : [ '20%', '80%' ],
                                    children : [
                                        {
                                            id: concatPluginName('ChapterSeconds'),
                                            label: 'Seconds',
                                            type: 'text'
                                        },
                                        {
                                            id: concatPluginName('ChapterTitle'),
                                            label: 'Title',
                                            type: 'text',
                                        }
                                    ]
                                },
                                {
                                    id: concatPluginName('AddChapter'),
                                    type: 'button',
                                    label: 'Add Chapter',
                                    onClick: function(event) {
                                        // let dialogElements = this.getDialog().parts.contents.$;
                                        let dialog = this.getDialog();

                                        const secondsFieldname = concatPluginName('ChapterSeconds');
                                        const titleFieldname = concatPluginName('ChapterTitle');

                                        let chapterSeconds = dialog.getValueOf(dialogId, secondsFieldname);
                                        let chapterTitle = dialog.getValueOf(dialogId, titleFieldname);

                                        if (!validateChapter(chapterSeconds, chapterTitle)) {
                                            return false;
                                        }

                                        chapterSeconds = getValidSeconds(chapterSeconds);
                                        chapterTitle = getValidChapterTitle(chapterTitle);

                                        let rawChapters = dialog.getValueOf(dialogId, concatPluginName('Chapters'));

                                        let updatedChapters = rawChapters.concat(`${chapterSeconds}|${chapterTitle}\n`);

                                        // Source: https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_dialog.html#method-setValueOf
                                        dialog.setValueOf(dialogId, concatPluginName('Chapters'), updatedChapters);

                                        // Clean chapter inputs
                                        dialog.setValueOf(dialogId, secondsFieldname, '');
                                        dialog.setValueOf(dialogId, titleFieldname, '');
                                    }
                                },
                                {
                                    id: concatPluginName('Chapters'),
                                    type: 'textarea',
                                    label: 'Chapters (one chapter per line. e.g. 60|Title 1)',
                                    setup: function (widget) {
                                        // let chapters = getVideoElement(widget).getAttribute('chapters');
                                        // if (chapters === null) {
                                        //     chapters = '';
                                        // }
                                        // let rawChapters = chapters.split(';').join('\n');
                                        // this.setValue(rawChapters);

                                        this.disable();
                                    },
                                    commit: function (widget) {
                                        let rawChapters = this.getValue();

                                        // Return if value is empty
                                        if (isEmptyField(rawChapters)) {
                                            return false;
                                        }

                                        let chapters = rawChapters.split('\n');
                                        // getVideoElement(widget).setAttribute('chapters', chapters.join(';'));

                                        const contentSeparator = '|';
                                        let validChapters = chapters.filter((value) => {
                                            return value.includes(contentSeparator);
                                        }).map((value) => {
                                            let chapter = value.split(contentSeparator);
                                            return {
                                                seconds: chapter.shift(),
                                                title: chapter.shift()
                                            }
                                        });

                                        let videoChaptersElement = getVideoElement(widget)
                                            .querySelector('.video-chapters');

                                        const recreateChapters = () => {
                                            let chapterElements = videoChaptersElement
                                                .querySelectorAll(chapterSelector);

                                            videoChaptersElement.innerHTML = '';

                                            chapterElements.forEach((chapterElement) => {
                                                videoChaptersElement.append(chapterElement);
                                            });
                                        };

                                        recreateChapters();

                                        validChapters.forEach((chapter) => {
                                            let chapterHtml = buildChaptersTemplate(chapter.seconds, chapter.title);
                                            let chapterElement = createElementFromString(chapterHtml);

                                            videoChaptersElement.append(chapterElement);
                                        });

                                        // Refresh wysiwyg widget
                                        let timeOutPromise = new Promise(
                                            resolve => setTimeout(resolve, 1000)
                                        );

                                        timeOutPromise.then(() => {
                                            widget.editor.setMode('source');
                                            widget.editor.setMode('wysiwyg');
                                        });
                                    },
                                    // validate: function(widget) {
                                    //     console.log(this);
                                    //     return false;
                                    //
                                    //     let videoChaptersElement = getVideoElement(widget)
                                    //         .querySelector('.video-chapters');
                                    //
                                    //     let countChapters = videoChaptersElement
                                    //         .querySelectorAll('.video-chapter')
                                    //         .length;
                                    //
                                    //     if (countChapters < 1) {
                                    //         alert('Video content must have at least one chapter.');
                                    //         return false;
                                    //     }
                                    // }
                                },
                                {
                                    id: concatPluginName('Layout'),
                                    type: 'select',
                                    label: 'Layout',
                                    // items e.g. [['horizontal'], ['vertical']]
                                    items: layouts.map((layout) => {
                                        return [layout.name];
                                    }),
                                    setup: baseSetup('layout'),
                                    commit: function(widget) {
                                        let layoutName = this.getValue();

                                        getVideoElement(widget).setAttribute('layout', layoutName);

                                        const replaceElementClass = (element,  classToRemove, classToAdd) => {
                                            if(element.classList.contains(classToRemove)) {
                                                element.classList.remove(classToRemove);
                                            }

                                            if(!element.classList.contains(classToAdd)) {
                                                element.classList.add(classToAdd);
                                            }
                                        };

                                        const toggleLayout = (videoPreviewElement, chaptersElement) => {
                                            let layout = horizontalLayout;
                                            let otherLayout = verticalLayout;

                                            if (layoutName === verticalLayout.name) {
                                                layout = verticalLayout;
                                                otherLayout = horizontalLayout;
                                            }

                                            replaceElementClass(
                                                videoPreviewElement,
                                                otherLayout.videoContainer,
                                                layout.videoContainer
                                            );

                                            replaceElementClass(
                                                chaptersElement,
                                                otherLayout.chaptersContainer,
                                                layout.chaptersContainer
                                            );
                                        };

                                        let chaptersElement = getVideoElement(widget)
                                            .querySelector('.video-chapters');

                                        let videoPreviewElement = getVideoElement(widget)
                                            .querySelector('.video-preview-placeholder').parentElement;

                                        toggleLayout(videoPreviewElement, chaptersElement);
                                    },
                                    validate: function() {
                                        if (!baseValidation()) {
                                            return false;
                                        }

                                        let layoutName = this.getValue();

                                        const layoutExists = layouts.find((layout) => {
                                            return layout.name === layoutName;
                                        });

                                        if (typeof layoutExists === 'undefined') {
                                            alert(`Layout '${layoutName}' does not exist.`);
                                            return false;
                                        }

                                        return true;
                                    }
                                }
                            ]
                        }
                    ]
                }
            };

            const iconUrl = getResourcePath('images/icon.png');

            const dialogName = concatPluginName('Dialog');
            CKEDITOR.dialog.add(dialogName, videoContentDialog);

            const widgetName = concatPluginName('Widget');

            editor.widgets.add(widgetName, {
                dialog: dialogName,
                template:
                   `<section class="video-container">
                        <${videoContentTagName} 
                            video-url=""
                            layout="horizontal"
                            authors=""
                            references="">
                                <div class="video-content-placeholder">
                                    <div class="video-icon fa fa-vimeo fa-3x"></div>
                                </div>
                                <h6 class="video-title">video title</h6>
                                ${videoContentsPreview}
                        </${videoContentTagName}>
                    </section>`,
                editables: {
                    title: {
                        selector: '.video-title',
                        allowedContent: 'strong em'
                    },
                    chapters: {
                        selector: '.video-chapters',
                        allowedContent: {
                            div: {
                                classes: [chapterClass, chapterSecondsClass, chapterTitleClass]
                            }
                        }
                    }
                },
                allowedContent: {
                    section: {
                        classes: true
                    },
                    'video-content': {
                        attributes: true,
                        classes: true,
                    },
                    h6: {
                        classes: ['video-title']
                    },
                    div: {
                        attributes: true,
                        // classes: ['video-content-placeholder', 'video-icon', 'video-chapters']
                        classes: true
                    }
                },
                requiredContent: 'section(video-container)',
                upcast: function( element ) {
                    return element.name === 'section' && element.hasClass( 'video-container' );
                }
            });

            editor.ui.addButton('VideoContent', {
                label: 'Embed Video Content',
                toolbar: 'insert',
                command: widgetName,
                icon: iconUrl
            });
        }
    });
})();
