# ckeditor-videocontent-plugin

Plugin for ckeditor to insert and edit `video-content` elements.

## Install

1. Add package to dependencies `package.json`

```json
{
  "dependencies": {
    "ckeditor-videocontent-plugin": "git+git@bitbucket.org:infomedica/ckeditor-videocontent-plugin.git#0.0.3"
  }
}
```

2. Run `npm install`

3. Use your module bundler to copy the `ckeditor-videocontent-plugin` 
   folder inside ckeditor's plugin folder `ckeditor/plugins/videocontent`.

4. Add `videocontent` to plugins in `ckeditor/config.js`

```js
config.extraPlugins = 'videocontent';
```

## Suggested packages

```json
{
  "bootstrap": "^3.4.0",
  "font-awesome": "^4.7.0"
}
```